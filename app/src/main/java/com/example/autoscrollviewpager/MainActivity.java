package com.example.autoscrollviewpager;

import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import com.example.autoscrollviewpager.ui.main.AutoScrollPagerAdapter;
import com.google.android.material.tabs.TabLayout;

public class MainActivity extends AppCompatActivity {
  private static final int AUTO_SCROLL_THRESHOLD_IN_MILLI = 1000;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);
    AutoScrollPagerAdapter autoScrollPagerAdapter =
        new AutoScrollPagerAdapter(getSupportFragmentManager());
    AutoScrollViewPager viewPager = findViewById(R.id.view_pager);
    viewPager.setAdapter(autoScrollPagerAdapter);
    TabLayout tabs = findViewById(R.id.tabs);
    tabs.setupWithViewPager(viewPager);

    // start auto scroll
    viewPager.startAutoScroll();
    // set auto scroll time in mili
    viewPager.setInterval(AUTO_SCROLL_THRESHOLD_IN_MILLI);
    // enable recycling using true
    viewPager.setCycle(true);
  }
}